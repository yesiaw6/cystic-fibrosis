import {Op} from "sequelize";

const {SECRET_KEY} = require('../../shared/constants');
const ApiError = require('../../error');
const { User } = require('../../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const generateJWT = async (id: number, email: string, name: string) => {
  return await jwt.sign({ id, email, name }, SECRET_KEY, { expiresIn: '24h' });
};

class AuthControllerClass {
  async registration(req: any, res: any, next: any) {
    const { email, password, name } = req.body;
    if (!email || !password) {
      return next(ApiError.badRequest('Неккоректный email или password'));
    }
    const candidate = await User.findOne({
      where: {
        [Op.or]: [
          { name },
          { email }
        ]
      }
    })
    if (candidate) {
      return next(ApiError.badRequest('Пользователь с таким email или name уже существует'))
    }
    const currentName = await User.findOne({ where: { name } });
    if (currentName) {
      return next(ApiError.badRequest('Это имя уже занято'))
    }
    const hashPassword = await bcrypt.hash(password, 3);
    const user = await User.create({ email, name, password: hashPassword });
    const token = await generateJWT(user.id, user.email, user.name);
    return res.json({ token });
  }

  async authorization(req: any, res: any, next: any) {
    const { email, password } = req.body;
    const user = await User.findOne({ where: { email } });
    if (!user) {
      return next(ApiError.internal('Пользователь не найден'));
    }
    const comparePassword = bcrypt.compareSync(password, user.password);
    if (!comparePassword) {
      return next(ApiError.internal('Неверный пароль'));
    }
    const token = await generateJWT(user.id, user.email, user.name);
    return res.json({ token });
  }
}

const AuthController = new AuthControllerClass();

export {
  AuthController,
};

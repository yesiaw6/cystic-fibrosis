import {NextFunction, Request, Response} from "express";
const { Dish } = require('../../models');
const ApiError = require('../../error');
const jwt = require('jsonwebtoken');
const {SECRET_KEY} = require('../../shared/constants');

class DishControllerClass {
  async createDish(req: Request, res: Response, next: NextFunction) {
    const { name, weight, tablet, fats } = req.body;
    const { authorization } = req.headers;
    if (authorization) {
      try {
        const { id } = jwt.verify(authorization, SECRET_KEY);

        const dish = await Dish.findOne({ where: { name, userId: id } });
        if (dish) {
          return next(ApiError.badRequest('Блюдо с таким именем уже существует'));
        }
        const newDish = await Dish.create({ name, weight, userId: id, tablet, fats });
        res.json(newDish);
      } catch(e) {
       return next(ApiError.badRequest(e));
      }
    } else {
      return next(ApiError.unauthorized())
    }
  }

  async getDishes(req: Request, res: Response, next: NextFunction) {
    const { authorization } = req.headers;
    if (authorization) {
      try {
        const { id } = jwt.verify(authorization, SECRET_KEY);
        const dishes = await Dish.findAll({ where: { userId: id } });
        return res.json({ dishes });
      } catch (e) {
        return next(ApiError.badRequest(e));
      }
    } else {
      next(ApiError.unauthorized())
    }
  }
}

const DishController = new DishControllerClass();

export {
  DishController,
}
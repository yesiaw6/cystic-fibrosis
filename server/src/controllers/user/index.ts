import {Request, Response} from "express";
const ApiError = require('../../error');
const { User } = require('../../models');
const {SECRET_KEY} = require('../../shared/constants');

const jwt = require('jsonwebtoken');

class UserControllerClass {
  async updateUser(req: Request, res: Response, next: any): Promise<object> {
    const { authorization } = req.headers;
    if (authorization) {
      try {
        const { coefficient } = req.body;
        const user = jwt.verify(authorization, SECRET_KEY);
        const { id: curId } = user;
        await User.update({ coefficient }, {
          where: {
            id: curId,
          }
        });
        const { id, name, email } = await User.findOne({ where: { id: curId } });
        return res.json({ success: true, user: { id, name, email, coefficient } });
      } catch (e) {
        return next(ApiError.badRequest(e));
      }
    }
    return next(ApiError.badRequest('Не авторизован'));
  }
  async getUser(req: Request, res: Response, next: any): Promise<object> {
    const { authorization } = req.headers;
    if (authorization) {
      try {
        const { id: curId } = jwt.verify(authorization, SECRET_KEY);
        const { id, name, email, coefficient } = await User.findOne({ where: { id: curId } });
        return res.json({ id, name, email, coefficient });
      } catch (e) {
        return next(ApiError.badRequest(e));
      }
    }
    return res.json({})
  }

}

const UserController = new UserControllerClass();

export { UserController };

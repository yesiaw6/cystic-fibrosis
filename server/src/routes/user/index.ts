import {UserController} from "../../controllers/user";

const Router = require('express');
const router = new Router();

router.get('/user', UserController.getUser);
router.post('/user/update', UserController.updateUser);

export {
  router as UserRouter,
};
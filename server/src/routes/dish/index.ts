import {DishController} from "../../controllers/dish";

const Router = require('express');
const router = new Router();



router.post('/create_dish', DishController.createDish);
router.get('/dishes', DishController.getDishes);

export {
  router as DishRouter,
};
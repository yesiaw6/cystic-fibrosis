const Router = require('express');
const router = new Router();


import { AuthController } from "../../controllers/auth";

router.post('/registration', AuthController.registration);
router.post('/authorization', AuthController.authorization);

export {
  router as AuthRouter,
};
import {DishRouter} from "./dish";

const Router = require('express');
const router = new Router();

import { AuthRouter } from "./auth";
import { UserRouter } from "./user";

router.use('/auth', AuthRouter);
router.use(UserRouter);
router.use(DishRouter);

export {
  router as routes,
};
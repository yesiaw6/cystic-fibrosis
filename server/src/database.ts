const { Sequelize } = require('sequelize');

const DB_NAME = 'CF';
const DB_USER = 'postgres';
const DB_PASSWORD = 'root';
const DB_HOST = 'localhost';
const DB_PORT = 8080;

module.exports = new Sequelize(
  DB_NAME,
  DB_USER,
  DB_PASSWORD,
  {
    dialect: 'postgres',
    host: DB_HOST,
    port: DB_PORT,
  }
)
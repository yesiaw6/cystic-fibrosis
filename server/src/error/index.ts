import {Error} from "sequelize";

class ApiError extends Error{
  message: string;
  status: number;

  constructor(status: any, message: string) {
    super();
    this.status = status;
    this.message = message;
  }

  static badRequest(message: string){
    return new ApiError(400, message);
  }

  static internal(message: string){
    return new ApiError(403, message);
  }
  static unauthorized() {
    return new ApiError(401, 'User unauthorized');
  }
}

module.exports = ApiError;

export { };
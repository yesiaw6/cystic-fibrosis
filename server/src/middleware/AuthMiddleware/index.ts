const jwt = require('jsonwebtoken');
const ApiError = require('../../error');
const {SECRET_KEY} = require('../../shared/constants');

module.exports = function (req: any, res: any, next: any) {
  if (req === 'OPTIONS') {
    next();
  }
  try {
    const [_, token] = req.headers.authorization?.split(' ') || [];
    if (!token) {
      res.status(401).json({ message: 'Не авторизован' });
    }
    try {
      const decodedToken = jwt.verify(token, SECRET_KEY);
      next();
    } catch {
      res.status(401).json({ message: 'Не авторизован' });
    }
  } catch (e) {
    next(ApiError.badRequest(e.message));
  }

};

export {};

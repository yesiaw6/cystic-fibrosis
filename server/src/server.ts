const express = require('express');
const cors = require('cors');

const sequelize = require('./database');
const models = require('./models');
const errorHandler = require('./middleware/ErrorHandlingMiddleware');
const { routes } = require('./routes');
// import * as cookieParser from 'cookie-parser';


const PORT = 8000;
const app = express();

app.use(cors());
app.use(express.json());
app.use('/api', routes);
app.use(errorHandler);

// sequelize.sync({ force: true }).then(() => {
//   console.log('Database tables synchronized');
// }).catch((error: any) => {
//   console.error('Error synchronizing tables:', error);
// });

const start = async () => {
  try {
    await sequelize.authenticate();
    await sequelize.sync();
    app.listen(PORT, () => {
      console.log('Starting in', PORT);
    });
  } catch (e) {
    console.log(e);
  }
};

start();

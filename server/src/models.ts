const sequelizeDB = require('./database');
const { DataTypes } = require('sequelize');

const User = sequelizeDB.define('user', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
  email: { type: DataTypes.STRING, unique: true },
  name: { type: DataTypes.STRING, unique: true, allowNull: true },
  coefficient: { type: DataTypes.FLOAT, allowNull: true },
  password: { type: DataTypes.STRING },
});
const Dish = sequelizeDB.define('dish', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
  name: { type: DataTypes.STRING, unique: false, allowNull: false },
  weight: { type: DataTypes.FLOAT, allowNull: true },
  fats: { type: DataTypes.FLOAT, allowNull: true },
  tablet: { type: DataTypes.FLOAT, allowNull: true },
});
const Tablet = sequelizeDB.define('tablet', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
  name: { type: DataTypes.STRING, allowNull: false },
  count: { type: DataTypes.FLOAT, allowNull: false },
  description: { type: DataTypes.STRING },
});

User.hasMany(Dish);
Dish.belongsTo(User, { foreignKey: 'userId' });

Dish.hasMany(Tablet);
Tablet.belongsTo(Dish, { foreignKey: 'dishId' });

module.exports = {
  User, Dish, Tablet,
}

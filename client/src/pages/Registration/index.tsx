import React from 'react';
import { Form } from 'rsuite';
import { useI18n } from '@shared/hooks';
import { useForm, FormProvider, SubmitHandler } from 'react-hook-form';
import { useAlertMessage } from '@shared/ui';
import { useRegistrationMutation } from '@entities/user/api';
import { useAppDispatch } from '@shared/store/hooks';
import { userSlice } from '@entities/user/model';
import { useNavigate } from 'react-router-dom';
import cx from './styles.module.css';
import { RegistrationForm } from './ui';

type RegistrationFormValues = {
  name: string;
  email: string;
  password: string;
};

export function Registration() {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { t } = useI18n();
  const { setToken } = userSlice.actions;
  const alert = useAlertMessage();
  const methods = useForm<RegistrationFormValues>({
    defaultValues: {
      name: '',
      email: '',
      password: '',
    },
  });
  const { handleSubmit } = methods;
  const [registration] = useRegistrationMutation();
  const onSubmit: SubmitHandler<RegistrationFormValues> = async (data) => {
    const result = await registration(data);
    if ('error' in result) {
      const { error }: { error: any } = result;
      alert({ text: error?.data?.message || 'Ошибка', type: 'error' });
      return;
    }
    if ('data' in result) {
      const { data: { token } } = result;
      if (token) {
        dispatch(setToken(token));
        navigate('/');
      }
    }
  };

  return (
    <div className={cx.container}>
      <h3>{t('pages.registration.title')}</h3>
      <FormProvider {...methods}>
        <Form
          onSubmit={(e: any) => handleSubmit(onSubmit)(e)}
        >
          <RegistrationForm />
        </Form>
      </FormProvider>
    </div>
  );
}

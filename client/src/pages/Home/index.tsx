import React, { useState } from 'react';
import { Placeholder } from 'rsuite';
import { NavLink } from 'react-router-dom';
import { useGetDishesQuery } from '@entities/Dish/api';
import { Dish } from '@entities/Dish/types';
import { useI18n } from '@shared/hooks';
import { classNames } from '@shared/lib';
import { Input } from '@shared/ui';
import styles from './styles.module.css';

const cx = classNames.bind(styles);
export function Home() {
  const [search, setSearch] = useState('');
  const { t } = useI18n();
  const { data: dishData, isLoading, isError } = useGetDishesQuery('');
  const { dishes = [] }: { dishes: Dish[] } = dishData || {};
  if (isLoading || isError) {
    return <Placeholder.Paragraph />;
  }
  const searchedDishes = dishes
    .filter(({ name }) => name.toLowerCase().includes(search.toLowerCase()));
  return (
    <div className={cx('container')}>
      <h1 className={cx('title')}>{t('page.home.title')}</h1>
      <div className={cx('header_actions')}>
        <Input
          value={search}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setSearch(e.target?.value);
          }}
          className={cx('action_input')}
          placeholder={t('search')}
        />
        <NavLink to="/create-dish" className={cx('create_button')}>
          {t('page.create_dish.button')}
        </NavLink>
      </div>

      <div className={cx('dishes_list')}>
        {searchedDishes.map(({
          id, name, weight, tablet, fats,
        }) => (
          <div className={cx('dish')} key={id}>
            <span>
              {t('page.create_dish.name')}
              :
              {' '}
              {name}
            </span>
            <span>
              {t('page.create_dish.weight')}
              :
              {' '}
              {weight}
            </span>
            <span>
              {t('page.create_dish.fats')}
              :
              {' '}
              {fats}
            </span>
            <span>
              {t('page.create_dish.tablet')}
              :
              {' '}
              {tablet}
            </span>
          </div>
        ))}
        {!searchedDishes.length && !search ? (
          <h3>{t('page.create_dish.alert')}</h3>
        ) : null}
      </div>
    </div>
  );
}

import React from 'react';
import { CreateDish } from '@features/Dish';

export function CreateDishPage() {
  return (
    <CreateDish />
  );
}

import React from 'react';
import { Form } from 'rsuite';
import { useI18n } from '@shared/hooks';
import { useForm, FormProvider, SubmitHandler } from 'react-hook-form';
import { useAuthorizationMutation } from '@entities/user/api';
import { userSlice } from '@entities/user/model';
import { useAlertMessage } from '@shared/ui';
import { useAppDispatch } from '@shared/store/hooks';
import { useNavigate } from 'react-router-dom';
import { AuthorizationForm } from './ui';
import cx from './styles.module.css';

type AuthorizationFormValues = {
  email: string;
  password: string;
};

export function Authorization() {
  const { t } = useI18n();
  const { setToken } = userSlice.actions;
  const alert = useAlertMessage();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const methods = useForm<AuthorizationFormValues>({
    defaultValues: {
      email: '',
      password: '',
    },
  });
  const { handleSubmit } = methods;
  const [authorization] = useAuthorizationMutation();
  const onSubmit: SubmitHandler<AuthorizationFormValues> = async (data) => {
    const result = await authorization(data);
    if ('error' in result) {
      const { error }: { error: any } = result;
      alert({ text: error?.data?.message || 'Ошибка', type: 'error' });
      return;
    }
    if ('data' in result) {
      const { data: { token } } = result;
      if (token) {
        dispatch(setToken(token));
        navigate('/');
      }
    }
  };

  return (
    <div className={cx.container}>
      <h3>{t('pages.authorization.title')}</h3>
      <FormProvider {...methods}>
        <Form
          onSubmit={(e: any) => handleSubmit(onSubmit)(e)}
        >
          <AuthorizationForm />
        </Form>
      </FormProvider>
    </div>
  );
}

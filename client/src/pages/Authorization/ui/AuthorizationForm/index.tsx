import React from 'react';
import { classNames } from '@shared/lib';
import { Button, ButtonToolbar, Form } from 'rsuite';
import { useFormContext, Controller } from 'react-hook-form';
import { useI18n } from '@shared/hooks';
import { Link } from 'react-router-dom';
import { REGISTRATION_PAGE } from '@app/routes/AuthRoutes';
import styles from './styles.module.css';

const cx = classNames.bind(styles);

export function AuthorizationForm() {
  const { t } = useI18n();
  const { control, reset } = useFormContext();
  return (
    <div className={cx('container')}>
      <Form.Group controlId="email">
        <Controller
          control={control}
          name="email"
          rules={{ required: true }}
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <>
              <Form.ControlLabel>{t('email')}</Form.ControlLabel>
              <Form.Control type="email" value={value} name="email" onChange={onChange} />
              {error ? (
                <Form.HelpText>{t('required.field')}</Form.HelpText>
              ) : null}
            </>
          )}
        />
      </Form.Group>
      <Form.Group controlId="password">
        <Controller
          control={control}
          name="password"
          rules={{ required: true }}
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <>
              <Form.ControlLabel>{t('password')}</Form.ControlLabel>
              <Form.Control onChange={onChange} value={value} name="password" type="password" autoComplete="off" />
              {error ? (
                <Form.HelpText>{t('required.field')}</Form.HelpText>
              ) : null}
            </>
          )}
        />
      </Form.Group>
      <Form.Group>
        <ButtonToolbar>
          <Button type="submit" appearance="primary">{t('submit')}</Button>
          <Button onClick={() => reset()} appearance="default">{t('cancel')}</Button>
          <Button appearance="link">
            <Link to={REGISTRATION_PAGE.path}>
              {t('pages.registration.title')}
            </Link>
          </Button>
        </ButtonToolbar>
      </Form.Group>
    </div>
  );
}

import { classNames } from '@shared/lib';
import { Button, useAlertMessage } from '@shared/ui';
import { Controller, useForm } from 'react-hook-form';
import { useI18n } from '@shared/hooks';
import { Form } from 'rsuite';
import React from 'react';
import { useUserUpdateMutation } from '@entities/user/api/userApi';
import styles from './styles.module.css';

const cx = classNames.bind(styles);

interface IContainer {
  data: {
    name: string,
    coefficient: number,
  },
  logout: () => void,
}

export function Container({ data, logout }: IContainer) {
  const { t } = useI18n();
  const { name, coefficient } = data || {};
  const {
    control, handleSubmit, formState: { isDirty }, reset,
  } = useForm({
    defaultValues: {
      coefficient: coefficient || '',
    },
  });
  const [updateUser] = useUserUpdateMutation();
  const alertMessage = useAlertMessage();
  const onSubmit = async (res: any) => {
    const response = await updateUser(res);
    if ('error' in response) {
      if (typeof response.error === 'object') {
        const { data: { message = '' } = {} } = response.error as { data: { message: string } } || {};
        alertMessage({ text: String(message) || t('error'), type: 'error' });
        return;
      }
      alertMessage({ text: t('error'), type: 'error' });
      return;
    }
    if ('data' in response) {
      const { user } = response.data || {};
      alertMessage({ text: t('success'), type: 'success' });
      reset({ coefficient: user.coefficient });
    }
  };
  return (
    <div className={cx('container')}>
      <h1>{t('page.profile.title')}</h1>
      <h2>
        {t('username')}
        :
        {' '}
        {name}
      </h2>
      <Button
        onClick={logout}
        type="button"
      >
        <h3>{t('logout')}</h3>
      </Button>
      <Form
        onSubmit={(e: any) => handleSubmit(onSubmit)(e)}
      >
        <Form.Group controlId="coefficient">
          <Controller
            control={control}
            name="coefficient"
            rules={{ required: true }}
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <>
                <Form.ControlLabel>{t('page.profile.coefficient')}</Form.ControlLabel>
                <Form.Control type="text" value={value} name="coefficient" onChange={onChange} />
                {error ? (
                  <Form.HelpText>{t('required.field')}</Form.HelpText>
                ) : null}
              </>
            )}
          />
        </Form.Group>
        <Button
          type="submit"
          appearance="primary"
          disabled={!isDirty}
        >
          {t('save')}
        </Button>
      </Form>
    </div>
  );
}

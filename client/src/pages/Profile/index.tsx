import React, { useEffect } from 'react';
import { useLogout } from '@features/User/useLogout';
import { useGetUserQuery } from '@entities/user/api';
import { Placeholder } from 'rsuite';

import { Container } from './ui';

export function Profile() {
  const { data, isLoading, isError } = useGetUserQuery('');
  const logout = useLogout();

  useEffect(() => {
    if (isError) {
      logout();
    }
  }, [isError]);
  if (isLoading || isError) {
    return <Placeholder.Paragraph />;
  }
  return (
    <Container data={data} logout={logout} />
  );
}

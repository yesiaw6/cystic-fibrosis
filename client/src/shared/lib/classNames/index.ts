import { Binding } from './type';

export function classNames(this: Binding, key: string) {
  return this[key];
}

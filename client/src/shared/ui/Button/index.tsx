import React from 'react';
import { Button as RButton } from 'rsuite';

export function Button({ ...props }) {
  return (
    <RButton block={false} appearance="ghost" {...props} />
  );
}

import React from 'react';
import { Message, useToaster } from 'rsuite';

interface IalertMessage {
  text: string,
  placement?: 'topCenter' | 'bottomCenter' | 'topStart' | 'topEnd' | 'bottomStart' | 'bottomEnd',
  duration?: number,
  type?: 'success' | 'warning' | 'error' | 'info',
}

export function useAlertMessage() {
  const toaster = useToaster();

  return ({
    text, placement = 'topCenter', type = 'success',
  }: IalertMessage) => {
    const message = (
      <Message showIcon type={type} closable>
        {text}
      </Message>
    );
    toaster.push(message, { placement });
  };
}

import React, { ReactElement } from 'react';
import { Nav } from 'rsuite';
import { useLocation, useNavigate } from 'react-router-dom';
import { useI18n } from '../../hooks';
import styles from './styles.module.css';
import { classNames } from '../../lib';

const cx = classNames.bind(styles);
export type NavItem = {
  eventKey: string,
  icon?: ReactElement,
  title: string,
};

export function Navbar(
  {
    navItems,
    ...props
  }: { navItems: NavItem[] },
) {
  const { pathname } = useLocation();
  const navigate = useNavigate();
  const onSelect = (key: string) => {
    navigate(key);
  };
  const active = pathname.split('/')[1];
  const { t } = useI18n();

  return (
    <Nav
      {...props}
      activeKey={active}
      onSelect={onSelect}
      appearance="tabs"
      reversed
    >
      {navItems.map(({ eventKey, title, icon }) => (
        <Nav.Item
          key={title}
          eventKey={eventKey}
          icon={icon}
          className={cx('nav_item')}
        >
          {t(title)}
        </Nav.Item>
      ))}
    </Nav>
  );
}

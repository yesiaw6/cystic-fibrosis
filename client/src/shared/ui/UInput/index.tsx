import React from 'react';
import { Input as RInput, InputGroup } from 'rsuite';
import SearchIcon from '@rsuite/icons/Search';

export function Input({ ...props }) {
  return (
    <InputGroup {...props}>
      <RInput {...props} />
      <InputGroup.Addon>
        <SearchIcon />
      </InputGroup.Addon>
    </InputGroup>
  );
}

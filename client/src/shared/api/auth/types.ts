export interface IAuthData {
  data: {
    email: string,
    password: string,
  }
}

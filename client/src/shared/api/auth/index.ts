import { instance as api } from '../instance';
import { IAuthData } from './types';

export default {
  authorization: ({ data }: IAuthData) => {
    api.post('/authorization', { body: JSON.stringify(data) });
  },
  registration: ({ data }: IAuthData) => {
    api.post('/registration', { body: JSON.stringify(data) });
  },
};

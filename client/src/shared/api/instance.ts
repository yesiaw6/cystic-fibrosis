import ky from 'ky';

export const instance = ky.extend({
  prefixUrl: 'http://localhost:8000/api',
  headers: {
    'Content-Type': 'application/json',
  },
  hooks: {
    beforeRequest: [
      function setHeaders(request) {
        request.headers.set('Authorization', `Bearer ${localStorage.getItem('authToken')}`);
      },
    ],
  },
});

import React, { ReactNode } from 'react';
import styles from './styles.module.css';
import { classNames } from '../../lib';

const cx = classNames.bind(styles);

export function MainLayout(
  { navigation, children }: { navigation: ReactNode, children: ReactNode },
) {
  return (
    <main className={cx('container')}>
      <div className={cx('content__container')}>
        {children}
      </div>
      <nav className={cx('navigation')}>
        {navigation}
      </nav>
    </main>
  );
}

export interface IJson {
  [key: string]: string,
}

export interface IModule {
  default: IJson;
}

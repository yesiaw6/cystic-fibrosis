import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IJson } from './type';

interface TranslateJsonState {
  json: IJson | null,
}

export const initialState: TranslateJsonState = {
  json: null,
};

export const translateJsonSlice = createSlice({
  name: 'translateJson',
  initialState,
  reducers: {
    setJson: (state, action: PayloadAction<IJson>) => {
      state.json = action.payload;
    },
  },
});

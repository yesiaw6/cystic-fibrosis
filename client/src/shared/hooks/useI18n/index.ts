import { useLayoutEffect } from 'react';
import { IModule } from './type';
import { useAppDispatch, useStateSelector } from '../../store/hooks';
import { translateJsonSlice } from './slice';

export function useTranslation() {
  const dispatch = useAppDispatch();
  const { setJson } = translateJsonSlice.actions;
  function getJson() {
    import('@shared/i18n/ru.json').then((res: IModule) => {
      dispatch(setJson(res.default));
    });
  }

  useLayoutEffect(() => {
    getJson();
  }, []);
}

export const useI18n = () => {
  const json = useStateSelector((state) => state.translateJson.json);

  const t = (key: string) => {
    if (json) {
      return json[key];
    }
    return '';
  };
  return { t };
};

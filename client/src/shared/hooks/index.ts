import { useI18n } from './useI18n';
import { translateJsonSlice } from './useI18n/slice';

export { useI18n, translateJsonSlice };

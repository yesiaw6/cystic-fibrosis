export const MENU_LIST = [
  {
    title: 'nav.title.home',
    eventKey: 'home',
  },
  {
    title: 'nav.title.profile',
    eventKey: 'profile',
  },
];

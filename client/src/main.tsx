import React from 'react';
import ReactDOM from 'react-dom/client';
import 'rsuite/dist/rsuite.min.css';
import './index.css';

import { Provider } from 'react-redux';
import { store } from '@app/store';
import { CustomProvider } from 'rsuite';
import { App } from './app/app';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <CustomProvider theme="dark">
    <Provider store={store}>
      <App />
    </Provider>
  </CustomProvider>,
);

import React from 'react';
import { useI18n } from '@shared/hooks';
import { classNames } from '@shared/lib';
import { Controller, useForm, useWatch } from 'react-hook-form';
import { Button, Form } from 'rsuite';
import { Dish } from '@entities/Dish/types';
import { useCreateDishMutation } from '@entities/Dish/api';
import { useAlertMessage } from '@shared/ui';
import { useGetUserQuery } from '@entities/user/api';
import { Link } from 'react-router-dom';
import styles from './styles.module.css';

const cx = classNames.bind(styles);
export function CreateDish() {
  const methods = useForm({
    defaultValues: {
      name: '',
      weight: '',
      fats: '',
      creon: 10000,
    },
  });
  const [createDish] = useCreateDishMutation();
  const { data: userData } = useGetUserQuery('');
  const { coefficient } = userData || {};
  const {
    control, handleSubmit, reset, formState: { isDirty },
  } = methods;
  const creon = useWatch({
    control,
    name: 'creon',
  });
  const weight = useWatch({
    control,
    name: 'weight',
  });
  const fats = useWatch({
    control,
    name: 'fats',
  });
  const { t } = useI18n();
  const alertMessage = useAlertMessage();
  const tablet = ((Number(fats) * (Number(weight) / 100)) * coefficient) / creon;
  async function onSubmit(data: Dish | any) {
    const response = await createDish({ ...data, tablet });
    if ('error' in response) {
      if (typeof response.error === 'object') {
        const { data: { message = '' } = {} } = response.error as { data: { message: string } } || {};
        alertMessage({ text: String(message) || t('error'), type: 'error' });
        return;
      }
      alertMessage({ text: t('error'), type: 'error' });
      return;
    }
    if ('data' in response) {
      alertMessage({ text: t('success'), type: 'success' });
      reset();
    }
  }
  return (
    <div className={cx('container')}>
      <div className={cx('header')}>
        <Link to="/dishes">
          {t('back')}
        </Link>
        <h1>{t('page.create_dish.title')}</h1>
      </div>
      <Form
        onSubmit={(e: any) => handleSubmit(onSubmit)(e)}
        className={cx('container')}
      >
        <Form.Group controlId="name">
          <Controller
            control={control}
            name="name"
            rules={{ required: true }}
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <>
                <Form.ControlLabel>{t('page.create_dish.name')}</Form.ControlLabel>
                <Form.Control type="text" value={value} name="name" onChange={onChange} />
                {error ? (
                  <Form.HelpText>{t('required.field')}</Form.HelpText>
                ) : null}
              </>
            )}
          />
        </Form.Group>
        <Form.Group controlId="weight">
          <Controller
            control={control}
            name="weight"
            rules={{ required: true }}
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <>
                <Form.ControlLabel>{t('page.create_dish.weight')}</Form.ControlLabel>
                <Form.Control onChange={onChange} value={value} name="weight" type="number" autoComplete="off" />
                {error ? (
                  <Form.HelpText>{t('required.field')}</Form.HelpText>
                ) : null}
              </>
            )}
          />
        </Form.Group>
        <Form.Group controlId="fats">
          <Controller
            control={control}
            name="fats"
            rules={{ required: true }}
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <>
                <Form.ControlLabel>{t('page.create_dish.fats')}</Form.ControlLabel>
                <Form.Control onChange={onChange} value={value} name="fats" type="number" autoComplete="off" />
                {error ? (
                  <Form.HelpText>{t('required.field')}</Form.HelpText>
                ) : null}
              </>
            )}
          />
        </Form.Group>
        <Form.Group controlId="creon">
          <Controller
            control={control}
            name="creon"
            render={({ field: { onChange, value } }) => (
              <>
                <Form.ControlLabel>{t('page.create_dish.creon')}</Form.ControlLabel>
                <Form.Control onChange={onChange} value={value} name="page.create_dish.creon" type="number" autoComplete="off" />
              </>
            )}
          />
        </Form.Group>
        <p>
          {t('page.create_dish.tablet')}
          :
          {' '}
          {tablet}
        </p>
        <Button
          type="submit"
          appearance="primary"
          disabled={!isDirty}
        >
          {t('create')}
        </Button>
      </Form>
    </div>
  );
}

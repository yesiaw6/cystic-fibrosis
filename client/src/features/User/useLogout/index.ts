import { userSlice } from '@entities/user/model';
import { userApi } from '@entities/user/api/userApi';
import { useAppDispatch } from '@shared/store/hooks';
import { useNavigate } from 'react-router-dom';
import { dishApi } from '@entities/Dish/api';

export const useLogout = () => {
  const dispatch = useAppDispatch();
  const navigation = useNavigate();
  const { logout } = userSlice.actions;
  return () => {
    dispatch(logout());
    dispatch(userApi.util.resetApiState());
    dispatch(dishApi.util.resetApiState());
    navigation('/');
  };
};

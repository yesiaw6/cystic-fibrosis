import { router as AuthRouter } from './AuthRoutes';
import { router as UserRouter } from './UserRoutes';

export { AuthRouter, UserRouter };

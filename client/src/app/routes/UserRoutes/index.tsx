import React from 'react';
import { Home } from '@pages/Home';
import { createBrowserRouter, Navigate, Outlet } from 'react-router-dom';
import { MainLayout } from '@shared/layouts/MainLayout';
import { Navbar } from '@shared/ui';
import { MENU_LIST } from '@shared/config/menu';
import { CreateDishPage } from '@pages/CreateDish';
import { Profile } from '@pages/Profile';

export const HOME_PAGE = {
  path: '/',
  element: (
    <MainLayout navigation={<Navbar navItems={MENU_LIST} />}>
      <Outlet />
    </MainLayout>
  ),
  children: [
    {
      path: '/',
      element: <Home />,
    },
    {
      path: 'home',
      element: <Home />,
    },
    {
      path: 'profile',
      element: <Profile />,
    },
    {
      path: 'create-dish',
      element: <CreateDishPage />,
    },
  ],
};
export const DEFAULT_PAGE = {
  path: '*',
  element: <Navigate to="/" />,
};

export const USER_ROUTES = [HOME_PAGE, DEFAULT_PAGE];

export const router = createBrowserRouter(USER_ROUTES);

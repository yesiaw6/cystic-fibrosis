import React from 'react';
import { createBrowserRouter, Navigate } from 'react-router-dom';
import { Registration } from '@pages/Registration';
import { Authorization } from '@pages/Authorization';

export const REGISTRATION_PAGE = {
  path: '/registration',
  element: <Registration />,
};
export const AUTHORIZATION_PAGE = {
  path: '/',
  element: <Authorization />,
  children: [
    {
      path: 'authorization',
      element: <Authorization />,
    },
  ],
};
export const DEFAULT_PAGE = {
  path: '*',
  element: <Navigate to="/" />,
};

export const AUTH_ROUTES = [REGISTRATION_PAGE, AUTHORIZATION_PAGE, DEFAULT_PAGE];

export const router = createBrowserRouter(AUTH_ROUTES);

import { configureStore } from '@reduxjs/toolkit';
import { translateJsonSlice } from '@shared/hooks';
import { userSlice } from '@entities/user/model';
import { userApi } from '@entities/user/api/userApi';
import { dishApi } from '@entities/Dish/api';

export const store = configureStore({
  reducer: {
    [translateJsonSlice.name]: translateJsonSlice.reducer,
    [userSlice.name]: userSlice.reducer,
    [userApi.reducerPath]: userApi.reducer,
    [dishApi.reducerPath]: dishApi.reducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware()
    .concat(userApi.middleware)
    .concat(dishApi.middleware),
});

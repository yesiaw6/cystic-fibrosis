import React from 'react';

import { RouterProvider } from 'react-router-dom';
import { useTranslation } from '@shared/hooks/useI18n';
import { useStateSelector } from '@shared/store/hooks';
import cx from './app.module.css';
import { AuthRouter, UserRouter } from './routes';

const localVh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', `${localVh}px`);
window.addEventListener('resize', () => {
  const vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

export function App() {
  const json = useStateSelector((state) => state.translateJson.json);
  const token = useStateSelector((state) => state.user.token);
  useTranslation();
  if (!json) return null;
  return (
    <div className={cx.container}>
      {token ? (
        <RouterProvider router={UserRouter} />
      )
        : <RouterProvider router={AuthRouter} />}
    </div>
  );
}

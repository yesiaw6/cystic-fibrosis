import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type User = {
  id: number,
  email: string,
  name: string,
};
export interface UserState {
  token: string | null,
  user: User | null,
}

const initialState: UserState = {
  token: localStorage.getItem('token'),
  user: null,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setToken: (state, action: PayloadAction<string>) => {
      localStorage.setItem('token', action.payload);
      state.token = action.payload;
    },
    setUser: (state, action: PayloadAction<User>) => {
      state.user = action.payload;
    },
    logout: (state) => {
      localStorage.removeItem('token');
      state.user = null;
      state.token = null;
    },
  },
});

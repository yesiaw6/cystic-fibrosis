import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { BASE_URL } from '@shared/constants';
import { getToken } from '../../lib';

export const userApi = createApi({
  reducerPath: 'userApi',
  tagTypes: ['user'],
  baseQuery: fetchBaseQuery({
    baseUrl: `${BASE_URL}/api`,
  }),
  endpoints: (builder) => ({
    registration: builder.mutation({
      query: (data) => ({
        url: '/auth/registration',
        method: 'POST',
        body: data,
      }),
    }),
    authorization: builder.mutation({
      query: (data) => ({
        url: '/auth/authorization',
        method: 'POST',
        body: data,
      }),
    }),
    getUser: builder.query({
      query: (): any => ({
        url: '/user',
        method: 'GET',
        headers: {
          Authorization: getToken(),
        },
      }),
      providesTags: () => [{ type: 'user' }],
    }),
    userUpdate: builder.mutation({
      query: (data): any => ({
        url: '/user/update',
        method: 'POST',
        body: data,
        headers: {
          Authorization: getToken(),
        },
      }),
      invalidatesTags: [{ type: 'user' }],
    }),
  }),
});

export const {
  useRegistrationMutation,
  useGetUserQuery, useAuthorizationMutation, useUserUpdateMutation,
} = userApi;

import { createApi } from '@reduxjs/toolkit/query/react';
import { fetchBaseQuery } from '@reduxjs/toolkit/dist/query/react';
import { BASE_URL } from '@shared/constants';
import { getToken } from '../../user/lib';

export const dishApi = createApi({
  reducerPath: 'dishApi',
  tagTypes: ['Dishes'],
  baseQuery: fetchBaseQuery({
    baseUrl: `${BASE_URL}/api`,
  }),
  endpoints: (builder) => ({
    getDishes: builder.query({
      query: (): any => ({
        url: '/dishes',
        method: 'GET',
        headers: {
          Authorization: getToken(),
        },
      }),
      providesTags: (result = {}) => (result.dishes
        ? [
          ...result.dishes.map(({ id }: { id: number }) => ({ type: 'Dishes', id })),
          { type: 'Dishes', id: 'LIST' },
        ]
        : [{ type: 'Dishes', id: 'LIST' }]),
    }),
    createDish: builder.mutation({
      query: (data): any => ({
        url: '/create_dish',
        method: 'POST',
        body: data,
        headers: {
          Authorization: getToken(),
        },
      }),
      invalidatesTags: [{ type: 'Dishes', id: 'LIST' }],
    }),
  }),
});

export const { useCreateDishMutation, useGetDishesQuery } = dishApi;

export type Dish = {
  id: number,
  name: string;
  weight: number;
  tablet: number;
  fats: number;
};
